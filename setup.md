# Virtual Machine

There's a Vagrantfile in this repo. If you have Vagrant installed you can do the following.

`$ cd into this directory`

`$ vagrant up`

`$ vagrant ssh`

And you're in.

This sets up a new Ubuntu 16 VM and logs you into the machine really quickly.

# The OpenVAS Modules


#### openvas-scanner
Scanner module for the Open Vulnerability Assessment System (OpenVAS).

#### openvas-manager
Manager Module for the Open Vulnerability Assessment System (OpenVAS)

The OpenVAS Manager is the central service that consolidates plain vulnerability
scanning into a full vulnerability management solution. The Manager controls the
Scanner via OTP and itself offers the XML-based, stateless OpenVAS Management
Protocol (OMP). All intelligence is implemented in the Manager so that it is
possible to implement various lean clients that will behave consistently e.g.
with regard to filtering or sorting scan results. The Manager also controls
a SQL database (sqlite-based) where all configuration and scan result data is
centrally stored.

#### openvas-gsa
Greenbone Security Assistant (GSA) is GUI to the OpenVAS


# Setup & Install OpenVAS

`sudo add-apt-repository ppa:mrazavi/openvas`

`sudo apt-get update`

`sudo apt install sqlite3`

`sudo apt install openvas9` - It will ask to configure Redis Unix socket. Select yes and continue.

`sudo apt install texlive-latex-extra --no-install-recommends`

`sudo apt install texlive-fonts-recommended`

`sudo apt install libopenvas9-dev`

#### OpenVAS Vulnerability data

`sudo greenbone-nvt-sync`

`sudo greenbone-scapdata-sync` - (This takes a long time)

`sudo greenbone-certdata-sync`

#### Enable services to start on system boot

`systemctl enable openvas-scanner`

`systemctl enable openvas-manager`

`systemctl enable openvas-gsa`

#### Stop the Services

`sudo systemctl stop openvas-scanner`

`sudo systemctl stop openvas-manager`

`sudo systemctl stop openvas-gsa`

#### Configure GSA

By default it was setup to run over https and this was problematic so I did the following:

`sudo gsad --listen=0.0.0.0 --http-only --port=4000`

#### Start up the services

`sudo systemctl start openvas-scanner`

`sudo systemctl start openvas-manager`

`sudo systemctl start openvas-gsa`

#### Check OpenVAS processes

`sudo netstat -ntlp | grep LISTEN`

You should see one for `gsad` listening on port 4000

#### Rebuild the NVT's cache

`openvasmd --rebuild --progress`

#### GSA Web Interface

Access it by going to `http://localhost:3443` on your host machine.

Default login is `admin/admin`

Create a task by going to scans -> tasks

In the top left of the Tasks page there are 3 small buttons. If you rollover the purple one you can access the task wizard